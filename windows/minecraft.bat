cd %appdata%
if exist .minecraft\updates (
cd .minecraft
cd updates
update
cd ..
java -jar TLauncher.jar
) else (
powershell rm minecraft.zip
powershell wget https://gitlab.com/v57/minecraft/repository/archive.zip?ref=master -OutFile minecraft.zip
powershell -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('minecraft.zip', 'bar'); }
powershell mv bar/minecraft* bar/minecraft

powershell mv .minecraft .minecraft-old
powershell mv .tlauncher .tlauncher-old

powershell rm -r -f .tlauncher
powershell rm -r -f .minecraft

powershell mv bar/minecraft/minecraft .minecraft
powershell mv .minecraft/tlauncher .tlauncher

powershell rm -r -f bar
powershell rm minecraft.zip

cd .minecraft
java -jar TLauncher.jar
)
