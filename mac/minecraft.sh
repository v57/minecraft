cd ~/Library/Application\ Support/
if [ -d minecraft/updates ]
then
cd minecraft
updates/update.sh
java -jar TLauncher.jar
else
rm archive.zip?ref=master
wget https://gitlab.com/v57/minecraft/repository/archive.zip?ref=master
mkdir bar
bsdtar -x -f archive.zip?ref=master -C bar
mv bar/minecraft* bar/minecraft

rm -rf tlauncher
rm -rf minecraft

mv bar/minecraft/minecraft minecraft

rm -rf bar
rm archive.zip?ref=master

cd minecraft
java -jar TLauncher.jar
fi
